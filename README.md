# qute-layout-switcher
Keyboard layout switcher for qutebrowser, inspired by keymaps in vim.
##### Warning: this uses the qutebrowser extension api which is subject to change, so don't be surprised if it breaks sometime in the future!
### Installation:
1. Clone this repository into your qutebrowser config directory
2. Add this snippet to your config.py
   ```python
   import sys, os
   sys.path.append(os.path.join(sys.path[0], 'qute-layout-switcher'))
   config.source("qute-layout-switcher/qutebrowser.py")
   ```
### Usage
Use the command ```:switch-keyboard layout``` replacing layout with the layout you want to use. Or, you can use the default keybindings ```,ld``` for dvorak and ```,lq``` for qwerty, and following the same pattern for other keyboard layouts.

Currently supported keyboard layouts:
* Qwerty
* Dvorak
